<?php
/**
 * @file
 * Contains \Drupal\render_cache_control\PartialPlaceholderingRenderCache
 */

namespace Drupal\render_cache_control;


use Drupal\Core\Render\PlaceholderingRenderCache;

class PartialPlaceholderingRenderCache extends PlaceholderingRenderCache {

  public function get(array $elements) {
    \Drupal::moduleHandler()->alter('render_cache_control_pre_get', $elements);
    return parent::get($elements);
  }

  public function set(array &$elements, array $pre_bubbling_elements) {
    \Drupal::moduleHandler()->alter('render_cache_control_pre_set', $elements, $pre_bubbling_elements);
    return parent::set($elements, $pre_bubbling_elements);
  }

}
