<?php

/**
 * @file
 * Contains \Drupal\render_cache_control\RenderCacheControlServiceProvider.
 */

namespace Drupal\render_cache_control;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * Defines a service provider for the render_cache_control module.
 */
class RenderCacheControlServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $container->getDefinition('render_cache')->setClass('Drupal\render_cache_control\PartialPlaceholderingRenderCache');
  }

}

