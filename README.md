This isn't ready yet.

Basic idea:
* Implement ```hook_render_cache_control_pre_get_alter(&$elements)```.
* Implement ```hook_render_cache_control_pre_set_alter(&$elements, $pre_bubbling_elements)``` if necessary.
* Set ```$elements['#cache']['max-age'] = 0;``` on elements that are not adapted to render caching yet.
